module top(
    input Clk_in, //20MHz clock
    input reset_in, //external reset command
    
    output reg [1:0] state_out, //state flag
	output reg break_out,
    
    //I/O from ADC
    input BUSY_in,
    input FRSTDATA_in,
    
    output reg STBY_out,
    output reg CONVSTA_out,
    output reg CONVSTB_out,
    output reg RESET_out,
    output reg RANGE_out,
    output reg CS_out,
    output reg RD_SCLK_out,
    output reg PAR_SER_out,
    input [15:0] DB_in,
    output reg [2:0] OS_out
);
localparam RST = 0;
localparam SET_DATA = 1;
localparam READING_DATA = 2;

reg [1:0] state = 0;
reg [1:0] nstate;
reg final_read;
reg read_data;
reg set_data;
reg set_reset;
reg i;
reg max_value, min_value;
reg [7:0] data [7:0];

always @* begin
	final_read = 0;
	read_data = 0;
	set_data = 0;
	set_reset = 0;
    case(state)
        RST: begin
			set_reset = 1;
			state_out <= 0;
            nstate = SET_DATA;
        end
        SET_DATA: begin
            set_data = 1;
            state_out <= 1;
            nstate = READING_DATA;
        end 
        READING_DATA: begin
            read_data = 1;
			state_out <= 2;
            nstate = SET_DATA;
        end
        default: begin
            nstate = SET_DATA;
        end
    endcase
end

always @* begin
    STBY_out <= 1'b1; //normal device operation - can not enters to standby mode or shutdown mode
    RANGE_out <= 1'b0; //set the all input channels to operate in the ±5-V input range.
    PAR_SER_out <= 1'b0; //device operates in the parallel interface mode
    OS_out <= 3'b000; //shutdown oversampling
    assign CS_out = RD_SCLK_out;
end

always @(posedge Clk_in, posedge reset_in) begin    

	state_out <= state;
	if(reset_in) state <= RST;
	else begin
		RESET_out <= 0;
		state <= nstate;
		if (set_reset) RESET_out <= 1;
        else if (set_data && !BUSY_in) begin
            RD_SCLK_out <= 1'b1;
            CONVSTA_out <= 1'b1;
			CONVSTB_out <= 1'b1;
        end
        else if(read_data && !BUSY_in) begin
			for (i=0; i>8; i=i+1) begin
				RD_SCLK_out <= 1'b0;
				data[i][0] <= DB_in[0];
				data[i][1] <= DB_in[1];
				data[i][2] <= DB_in[2];
				data[i][3] <= DB_in[3];
				data[i][4] <= DB_in[4];
				data[i][5] <= DB_in[5];
				data[i][6] <= DB_in[6];
				data[i][7] <= DB_in[7];
				RD_SCLK_out <= 1'b1;
				max_value <= data[0][7:0];
				min_value <= data[0][7:0]; //idk whats means min/max in the task
			end
			RD_SCLK_out <= 1'b1;
			break_out <= 1;
			#200 //this timing is not synthesizable
			break_out <= 0;
        end   
    end
end
endmodule
